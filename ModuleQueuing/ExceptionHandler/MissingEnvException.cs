﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleQueuing.ExceptionHandler
{
    public class MissingEnvException : Exception
    {

        public MissingEnvException() : base() { }

        public MissingEnvException(string message) : base(message) { }

    }
}
