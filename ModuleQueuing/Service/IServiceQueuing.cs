﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModuleQueuing.Service
{
    interface IServiceQueuing<T>
    {
        void SendData(T message);

        void StartListen();
    }
}
