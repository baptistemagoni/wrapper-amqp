﻿using ModuleQueuing.Class;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace ModuleQueuing.Service
{
    /// <summary>
    /// Source : https://www.rabbitmq.com/tutorials/tutorial-one-dotnet.html
    /// </summary>
    public class ServiceQueuing<T> : IServiceQueuing<T>
    {

        #region Fields

        private IConnection _connection;
        private ConnectionFactory _factory;
        private QueuingConfig configQueuing;

        public event EventHandlerListen OnListenQueue;
        public delegate void EventHandlerListen(object sender, T receiveData);

        #endregion

        #region Constructors

        public ServiceQueuing()
        {
            configQueuing = new QueuingConfig();
        }

        #endregion

        #region Methods

        /// <summary>
        /// This function init the connection
        /// </summary>
        public void InitQueuing()
        {
            this.Open();
        }

        /// <summary>
        /// This function open new connection
        /// </summary>
        /// <returns></returns>
        public bool Open()
        {
            _factory = new ConnectionFactory() { Uri = new Uri(configQueuing.ConnectionString()) };
            _connection = _factory.CreateConnection();
            return _connection != null;
        }

        /// <summary>
        /// This function can send data in the queue 
        /// </summary>
        /// <param name="element"></param>
        public void SendData(T element)
        {
            using(IModel channel = _connection.CreateModel())
            {
                channel.QueueDeclare(queue: configQueuing.QueueName, durable: configQueuing.Durable, exclusive: configQueuing.Exclusive, autoDelete: false);
                byte[] body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(element));
                channel.BasicPublish(exchange: "", routingKey: configQueuing.QueueName, basicProperties: null, body: body);

            }
        }

        /// <summary>
        /// This function can start listening the queue
        /// </summary>
        public void StartListen()
        {
            IModel channel = _connection.CreateModel();
            EventingBasicConsumer consumer = new EventingBasicConsumer(channel);
            consumer.Received += Consumer_Received;
            channel.BasicConsume(queue: configQueuing.QueueName, autoAck: false, consumer: consumer);
        }

        #endregion

        #region Internals methods
        
        /// <summary>
        /// This function receive data and execute an event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="event"></param>
        private void Consumer_Received(object sender, BasicDeliverEventArgs @event)
        {
            var message = Encoding.UTF8.GetString(@event.Body.ToArray());
            try { this.OnListenQueue(this, JsonConvert.DeserializeObject<T>(message)); }
            catch (Exception) { }
        }
        
        #endregion
}
}
