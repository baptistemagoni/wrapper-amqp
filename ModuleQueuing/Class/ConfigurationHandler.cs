﻿using ModuleQueuing.ExceptionHandler;
using System;
using System.Linq;

namespace ModuleQueuing.Class
{
    public abstract class ConfigurationHandler
    {

        #region Constructors

        public ConfigurationHandler(string prefix)
        {
            LoadConfiguration(prefix);
        }

        #endregion

        #region Methods

        /// <summary>
        /// This function use reflection for aggragate attributes automaticaly from environement variable
        /// </summary>
        /// <param name="prefix"></param>
        private void LoadConfiguration(string prefix)
        {
            this.GetType().GetProperties().ToList().ForEach(property =>
            {
                string newValue = DotNetEnv.Env.GetString(prefix + property.Name.ToUpper());
                if (string.IsNullOrEmpty(newValue))
                    throw new MissingEnvException($"Not found env for attribute : {property.Name}");
                property.SetValue(this, Convert.ChangeType(newValue, property.PropertyType));
            });
        }

        #endregion
    }
}
