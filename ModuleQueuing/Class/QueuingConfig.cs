﻿namespace ModuleQueuing.Class
{
    public class QueuingConfig : ConfigurationHandler
    {

        private const string ENV_PREFIX = "AMQP_";

        /// <summary>
        /// Define schema (amqp, amqps)
        /// </summary>
        public string Schema { get; set; }

        /// <summary>
        /// Define username. Credentials for connection
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Define password. Credentials for connection
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Define the virtualhost (Optionnel) default Guest
        /// </summary>
        public string VirtualHost { get; set; }


        /// <summary>
        /// Define hostname. Credentials for connection
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Define port. Default port is 5671
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Define name of the queue. Need for get information from the queue
        /// </summary>
        public string QueueName { get; set; }

        /// <summary>
        /// Define if your queue is durable or not
        /// </summary>
        public bool Durable { get; set; }

        public bool Exclusive { get; set; }

        public QueuingConfig() : base(ENV_PREFIX) { }

        /// <summary>
        /// This function return connection string for connect on the system
        /// </summary>
        /// <returns></returns>
        public string ConnectionString()
        {
            return $"{Schema}://{UserName}:{Password}@{HostName}:{Port}/{VirtualHost}";
        }

    }
}
